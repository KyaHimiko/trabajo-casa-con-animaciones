using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toiletAnimacion : MonoBehaviour
{
    public GameObject toiletCap;
    public Animator Toilet;

    public void Start()
    {
        Toilet.GetBool("toiletActivated");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            Toilet.SetBool("toiletActivated", true);
            Debug.Log("activado");
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Toilet.SetBool("toiletActivated", false);
            Debug.Log("Desactivado");
        }
    }
}