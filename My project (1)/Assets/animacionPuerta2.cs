using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacionPuerta2 : MonoBehaviour
{
    public GameObject puertaSegunda;
    public Animator Puerta2;

    public void Start()
    {
        Puerta2.GetBool("isActivated2");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            Puerta2.SetBool("isActivated2", true);
            Debug.Log("activado");
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Puerta2.SetBool("isActivated2", false);
            Debug.Log("Desactivado");
        }
    }
}