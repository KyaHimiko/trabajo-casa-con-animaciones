using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacionFridge : MonoBehaviour
{
    public GameObject chairAnim;
    public Animator Chair;

    public void Start()
    {
        Chair.GetBool("chairActivated");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            Chair.SetBool("chairActivated", true);
            Debug.Log("activado");
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Chair.SetBool("chairActivated", false);
            Debug.Log("Desactivado");
        }
    }
}