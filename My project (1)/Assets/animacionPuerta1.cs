using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacionPuerta1 : MonoBehaviour
{
    public GameObject puertaPrimera;
    public Animator Puerta1;

    public void Start()
    {
        Puerta1.GetBool("isActivated");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            Puerta1.SetBool("isActivated", true);
            Debug.Log("activado");
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Puerta1.SetBool("isActivated", false);
            Debug.Log("Desactivado");
        }
    }
}